<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Portfolio extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $authUserToken = $this->MyModel->auth();
                $resp = $this->MyPortfolio->portfolio_all_data();
                json_output(200, $resp);
            }
        }
    }

    public function item($alias = false)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(3) == '') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client == true) {
                $authUserToken = $this->MyModel->auth();
                $resp = $this->MyPortfolio->portfolio_detail_data($id);
                json_output(200, $resp);
            }
        }
    }

}
