<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of blogs
 *
 * @author oleg_kosarev
 */
class Blog extends CI_Controller {

    public function __construct() {
        $this->fromPost = "posts";
        $this->fromCategory = "category";
        parent::__construct();
    }

    public function index() {
        $check_auth_client = $this->MyModel->check_auth_client();
        if ($check_auth_client == true) {
            $response = $this->MyModel->auth();
            if ($response['status'] == 200) {
                var_dump("this is index public function");
            }
        }
    }

    public function posts() {
        var_dump("this is all post public function");
    }

    public function post($id) {
        var_dump("this is $id post public function");
    }

    public function categories() {
        var_dump($this->uri->segment());
        if ($this->uri->segment('4') !== NULL) {
            var_dump("this is all category public function");
        } else {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        }
    }

    public function categorie($id) {
        var_dump("this is all posts from category $id");
    }

}
