<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    public $lang = "ru";

    public function find($limit = null, $offset = 0, $conditions = array())
    {

    }

    public function find_active()
    {
        if (!empty($this->uri->segment(3)) and !empty($this->uri->segment(4))) {
            $type = $this->uri->segment(3);
            if (!empty($this->uri->segment(4))) {
                $lang = $this->uri->segment(4);
            }
            $lang_array = array('*', $lang);
            $resp = $this->MyCategory->find_active($type, $lang_array);
            json_output(200, $resp);
        } else {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        }
    }

    public function create()
    {
 /*       if (is_numeric($this->input->get_request_header('id')) != false) {
            $user_id = $this->input->get_request_header('id');
            $check_user_role = $this->MyAuth->check_user_role($user_id, "create_blog_categories");
            $check_auth_client = $this->MyModel->check_auth_client();
            if ($check_auth_client === true and $check_user_role === true) {
                $response = $this->MyModel->auth();
                $respStatus = $response['status'];
                if ($response['status'] == 200) {
                    $category = null;
                    parse_str(file_get_contents("php://input"), $category);
                    $response = $this->MyCategory->create($category);
                    json_output(200, $response);
                }
            } else {
                //json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
            }
        } else {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        }*/
    }

    public function update($id)
    {
        $category = null;
        parse_str(file_get_contents("php://input"), $category);
        $resp = $this->MyCategory->update($category, $id);
        json_output(200, $resp);
    }

    public function delete($id)
    {
        //parse_str(file_get_contents("php://input"), $category);
        $resp = $this->MyCategory->delete($id);
        json_output(200, $resp);
    }

    public function find_by_id()
    {

    }

    public function find_by_slug()
    {

    }

    public function find_list()
    {
        if (!empty($this->uri->segment(3)) and !empty($this->uri->segment(4))) {
            $type = $this->uri->segment(3);
            if (!empty($this->uri->segment(4))) {
                $lang = $this->uri->segment(4);
            }
            $resp = $this->MyCategory->find_list($type, $lang);
            json_output(200, $resp);
        } else {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        }
    }

}
