<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Items extends CI_Controller
{

    public $lang = "ru";

    public function __construct()
    {
        parent::__construct();
    }

    public function find_active()
    {
        if (! empty($this->uri->segment(3))) {
            $type = $this->uri->segment(3);
            if (! empty($this->uri->segment(4))) {
                $lang = $this->uri->segment(4);
            } else {
                $lang = $this->lang;
            }
            $lang_array = array(
                '*',
                $lang
            );
            $resp = $this->MyItems->find_active($type, $lang_array);
            json_output(200, $resp);
        } else {
            json_output(400, array(
                'status' => 400,
                'message' => 'Bad request.'
            ));
        }
    }

    public function detail()
    {
        if (! empty($this->uri->segment(3)) and ! empty($this->uri->segment(4))) {
            $alias = $this->uri->segment(4);            
            $resp = $this->MyItems->detail($alias);
            json_output(200, $resp);
        } else {
            json_output(400, array(
                'status' => 400,
                'message' => 'Bad request.'
            ));
        }
    }
}

