<?php
class MyBlog extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        
    }
    
    public function getAllCategories() {
        
    }
    
    public function getAllCategorie($idCat) {
        
    }
    
    public function getAllPosts() {
        
    }
    
    public function getAllPostsCategorie($idCat){
        
    }
    
    public function commentsPostGet($idPost) {
        
    }
    
    public function commentsInsert($idPost, $name, $email, $comment, $parrentIdComments = null) {
        
    }
    
    public function commentsPublish($idComments) {
        
    }
    
    public function commentDelete($idComment) {
        
    }
}
