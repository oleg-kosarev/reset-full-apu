<?php

class MyAuth extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * check_auth_client domain and auth_key
     *
     * desk function check_auth_client
     *
     *
     * @return    booleen    depends on what the array contains
     */
    public function check_auth_client()
    {
        $client_service = $this->input->get_request_header('Client-Service');
        $auth_key = $this->input->get_request_header('Auth-Key');
        $q = $this->db
            ->select('auth_key,client_service')
            ->from('users')
            ->where('client_service ', $client_service)
            ->where('auth_key ', $auth_key)
            ->get()->row();
        if ($q == "") {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        } else {
            return true;
        }
    }

    public function check_user_role($users_id, $role_name)
    {
        $this->db->select('c.*');
        $this->db->join('usergroup_map c', 'pc.group_id=c.group_id');
        $this->db->where('c.user_id', $users_id);
        $this->db->where('pc.name', $role_name);
        $result = $this->db->get('users_role pc')->num_rows();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * login
     *
     * Get token
     *
     * @param    string
     * @param    string
     * @return    array    depends on what the array contains
     */
    public function login($username, $password)
    {
        $q = $this->db->select('password,id')->from('users')->where('username', $username)->get()->row();
        if ($q == "") {
            return array('status' => 204, 'message' => 'Username not found.');
        } else {
            $hashed_password = $q->password;
            $id = $q->id;
            if (hash_equals($hashed_password, md5($password))) {
                $last_login = date('Y-m-d H:i:s');
                $token = crypt(substr(md5(rand()), 0, 32), '');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('id', $id)->update('users', array('last_login' => $last_login));
                $this->db->insert('users_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error.');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200, 'message' => 'Successfully login.', 'id' => $id, 'token' => $token, 'expired_at' => $expired_at);
                }
            } else {
                echo "Wrong password";
                exit();
                return array('status' => 204, 'message' => 'Wrong password.');
            }
        }
    }

    public function logout()
    {
        $users_id = $this->input->get_request_header('User-ID', true);
        $token = $this->input->get_request_header('Authorization', true);
        $this->db->where('users_id', $users_id)->where('token', $token)->delete('users_authentication');
        return array('status' => 200, 'message' => 'Successfully logout.');
    }

    /**
     * auth
     *
     * cheack expired at token < now
     *
     * @return    array    depends on what the array contains
     */
    public function auth()
    {
        $users_id = $this->input->get_request_header('User-ID', true);
        $token = $this->input->get_request_header('Authorization', true);
        $q = $this->db->select('expired_at')->from('users_authentication')->where('users_id', $users_id)->where('token', $token)->get()->row();
        if ($q == "") {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
        } else {
            if ($q->expired_at < date('Y-m-d H:i:s')) {
                return json_output(401, array('status' => 401, 'message' => 'Your session has been expired.'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id', $users_id)->where('token', $token)->update('users_authentication', array('expired_at' => $expired_at, 'updated_at' => $updated_at));
                return array('status' => 200, 'message' => 'Authorized.');
            }
        }
    }

}
