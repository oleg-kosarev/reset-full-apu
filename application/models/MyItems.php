<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MyItems extends CI_Model
{

    public $table = 'posts';

    public function __construct()
    {
        parent::__construct();
    }

    public function find_active($type, $lang, $limit = null, $offset = 0, $q = null)
    {
        $arrItems = array();
        $this->db->select('posts.*,posts.slug as alias, users.username,categories.slug');
        $this->db->join('users', 'users.id = posts.user_id');
        $this->db->join('posts_categories', 'posts_categories.post_id = posts.id');
        $this->db->join('categories', 'posts_categories.category_id = categories.id');
        if ($q != null) {
            $this->db->like('title', $q);
        }
        $this->db->where('posts.status', 1);
        $this->db->where('posts.type', $type);
        $this->db->limit($limit, $offset);
        $this->db->order_by('posts.created', 'DESC');
        $this->db->order_by('posts.published_at', 'DESC');
        $query = $this->db->get($this->table);
        $items = $query->result_array();
        if (count($items) > 0) {
            switch ($type) {
                case "blog":
                    $arrItems = self::postsObj($items);
                    break;
                case "portfolio":
                    $arrItems = self::portfolioObj($items);
                    break;
            }
        }
        return $arrItems;
    }

    public function portfolioObj($items = array())
    {
        /* {title: 'vkBot', alias: 'vkBot', type: 'image', images: {thumb: 'vkBot_r.png', featured: 'vkBot.png'}, category: 'web-progrgramming'} */
        $i = 0;
        foreach ($items as $item) {
            $arrItems[$i]["id"] = $item["id"];
            $arrItems[$i]["title"] = $item["title"];
            $arrItems[$i]["alias"] = $item["alias"];
            $arrItems[$i]["images"]["featured_image"] = $item["featured_image"];
            $arrItems[$i]["images"]["thumb"] = $item["thumb"];
            $arrItems[$i]["category"] = $item["slug"];
            $i++;
        }
        return $arrItems;
    }

    public function postsObj($items = array())
    {
        $i = 0;
        foreach ($items as $post) {
            $postId = $post["id"];
            $arrItems[$i]["id"] = $post["id"];
            $arrItems[$i]["title"] = $post["title"];
            $arrItems[$i]["slug"] = $post["alias"];
            $arrItems[$i]["featured_image"] = $post["featured_image"];
            $arrItems[$i]["thumb"] = $post["thumb"];
            $arrItems[$i]["username"] = $post["username"];
            $arrItems[$i]["iconPost"] = $post["iconPost"];
            $arrItems[$i]["typePost"] = $post["typePost"];
            $arrItems[$i]["video"] = $post["video"];
            $arrItems[$i]["published_at"] = $post["published_at"];
            $arrItems[$i]["created"] = $post["created"];
            // $arrItems["items"][$i]["tags"] = $this->getAllTags($postId);
            $arrItems[$i]["preview_body"] = $post["preview_body"];
            $arrItems[$i]["hidden_body"] = $post["body"];
            $arrItems[$i]["slugCat"] = $post["slug"];
            $arrItems[$i]["clientName"] = $post["clientName"];
            $arrItems[$i]["urlProject"] = $post["urlProject"];
            $arrItems[$i]["uid"] = $post["user_id"];
            $i++;
        }
        return $arrItems;
    }

    public function detail($alias)
    {
        $this->db->select('posts.*,users.username');
        $this->db->join('users', 'users.id = posts.user_id');
        $this->db->where('posts.slug', $alias);
        $this->db->where('posts.status', 1);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $item = $query->result_object();
        if (empty($item)) {
            return false;
        } else {
            $typeEntity = $item[0]->type;
            switch ($typeEntity) {
                case "blog":
                    $arrItems = self::postsItem($item[0]);
                    break;
                case "portfolio":
                    $arrItems = self::portfolioItem($item[0]);
                    break;
            }
            return $arrItems;
        }
    }

    private function assets_entity($itemRetrun= array(), $entity_id)
    {
        $this->db->select("a.path");
        $this->db->join("assets a", "ap.id = a.id");
        $this->db->where("ap.post_id", $entity_id);
        $query = $this->db->get("assets_posts ap");
        $images = $query->result_object();
        if(count($images) > 0){
            $i = 1;
            foreach ($images as $item) {
                $itemRetrun[$i]["small"] = $item->path;
                $itemRetrun[$i]["medium"] = $item->path;
                $itemRetrun[$i]["big"] = $item->path;
                $itemRetrun[$i]["description"] = $item->path;
                $i++;                
            }
        }
        return $itemRetrun;
    }

    private function portfolioItem($item)
    {
        $assets_images = array(
            array (
                "small" => $item->featured_image,
                "medium" => $item->featured_image,
                "big" => $item->featured_image,
                "description" => $item->title
            )
        );
        $itemRetrun = array(
            "title" => $item->title,
            "clientName" => $item->clientName,
            "dateCompleate" => $item->published_at,
            "link" => $item->urlProject,
            "target" => "blank",
            "description" => $item->body,
            "title" => $item->title,
            "images" => self::assets_entity($assets_images, $item->id),
            "video" => array(),
        );
        return $itemRetrun;
    }
}
