<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MyPortfolio extends CI_Model {

    public function __construct() {
        $this->from = 'portfolio';
        parent::__construct();
    }

    public function portfolio_all_data() {
        $select = "id,title,images";
        return $this->db->select($select)->from($this->from)->order_by('id', 'desc')->get()->result();
    }

    public function portfolio_detail_data($alias) {
        return $this->db->select('id,title,clientId')->from($this->from)->where('alias', $alias)->get()->row();
    }

}
