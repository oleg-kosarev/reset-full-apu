<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MyCategory extends CI_Model
{

    public $table = 'categories';

    public function find($limit = null, $offset = 0, $conditions = array())
    {
        $categories = $this->db->where($conditions)->order_by('id', 'asc')->get($this->table, $limit, $offset)->result_array();
        return $categories;
    }

    public function find_active($type, $lang)
    {
        $this->db->select('c.*');
        $this->db->join('categories c', 'pc.category_id=c.id');
        $this->db->join('posts p', 'pc.post_id=p.id');
        $this->db->where('c.status', 1);
        $this->db->where('p.status', 1);
        $this->db->where("c.type=", $type);
        $this->db->where_in('lang', $lang);
        $this->db->group_by('pc.category_id');
        $this->db->order_by('c.name', 'asc');
        $categories = $this->db->get('posts_categories pc')->result_array();
        return $categories;
    }

    /*
    send post cat name
     */

    public function create($category)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        $category['slug'] = strtolower(str_replace(" ", "-", str_replace($rus, $lat, $category['name'])));
        $this->db->insert($this->table, $category);
        $resp = $this->db->insert_id();
        return  array("id"=>$resp);
    }

    public function update($category, $id)
    {    
        $category['slug'] = url_title($category['name'], '-', true);
        $this->db->where('id', $id);
        $this->db->update($this->table, $category);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function find_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->table, 1)->row_array();
    }

    public function find_by_slug($id)
    {
        $this->db->where('slug', $id);
        return $this->db->get($this->table, 1)->row_array();
    }

    public function find_list($type, $lang)
    {
        $this->db->order_by('name', 'asc');
        $this->db->where("type='$type'");
        $this->db->where("lang='*'");
        $this->db->or_where("lang='$lang'");

        $query = $this->db->get($this->table);
        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[$row['id']] = $row['name'];
            }
        }
        return $data;
    }

}
