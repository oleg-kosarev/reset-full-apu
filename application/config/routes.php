<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//admin routes
$route['admin/login']       = 'admin/login/index';
$route['admin/dashboard']       = 'admin/cpanel/index';



//api routers
//auth
$route['v(:num)/auth/login']['post']           = 'v$1/auth/login';
$route['v(:num)/auth/logout']['post']          = 'v$1/auth/logout';

//Category

$route['book/v(:num)']['get']          	  = 'v$1/book';
$route['book/detail/(:num)']['get']    = 'book/detail/$1';
$route['book/create']['post']   	   = 'book/create';
$route['book/update/(:num)']['put']    = 'book/update/$1';
$route['book/delete/(:num)']['delete'] = 'book/delete/$1';



$route['v(:num)/category/(:any)/(:any)']['get'] = 'v$1/category/find_active/$2';
$route['v(:num)/items/(:any)']['get'] = 'v$1/items/find_active/$2';

$route['v(:num)/items/detail/(:any)']['get'] = 'v$1/items/detail/$2';

$route['v(:num)/category']['post'] = 'v$1/category/create';
$route['v(:num)/category/(:num)']['put'] = 'v$1/category/update/$2';
$route['v(:num)/category/(:num)']['delete'] = 'v$1/category/delete/$2';


$route['default_controller'] = 'site';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
